/*
 * crc.c
 *
 *  Created on: Sep 27, 2022
 *      Author: zamek
 */



#include "crc.h"

#define ESP_LOG_LOCAL_LEVEL ESP_LOG_INFO
#include "esp_log.h"

#define TAG "crc"

#define POLYNOMIAL (0x1021)  //CCIT(0xffff)
#define POLYNOMIAL_MODBUS (0x00a1)

crc16_t crc_calc_buffer(char *data, size_t size){
	if (!(data&&size))
		return 0;

	crc16_t rmdr=0;
	unsigned int i=0;
	for (;i<size;i++)
		crc_add_byte(&rmdr, *(data+i));

	return rmdr;
}

void crc_add_byte(crc16_t *crc, char byte){
	int bits;

	*crc ^= ((uint16_t) byte) << 8;

	for(bits=8;bits>0;--bits)
		if (*crc & 0x8000)
			*crc=(*crc<<1) ^ POLYNOMIAL;
		else
			*crc=(*crc<<1);
	ESP_LOGD(TAG, "crc_add_byte, char:%x, crc:%x", byte, *crc);
}

void crc_modbus_add_byte(crc16_t *crc, char byte){
	int bits;
	*crc ^= ((uint16_t) byte);

	for(bits=8;bits>0;--bits)
		if (*crc & 1) {
			*crc>>=1;
			*crc^=POLYNOMIAL_MODBUS;
		}
		else
			*crc>>=1;
}

void crc_begin(crc16_t *crc){
	*crc=(crc16_t)0xffff;
}

void crc_modbus_begin(crc16_t *crc){
	*crc=(crc16_t)0xffff;
}


