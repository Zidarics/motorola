/* UART Events Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/uart.h"
#include "driver/gpio.h"
#include "esp_log.h"

#include "crc.h"

static const char *TAG = "uart_events";

/**
 * This example shows how to use the UART driver to handle special UART events.
 *
 * It also reads data from UART0 directly, and echoes it to console.
 *
 * - Port: UART0
 * - Receive (Rx) buffer: on
 * - Transmit (Tx) buffer: off
 * - Flow control: off
 * - Event queue: on
 * - Pin assignment: TxD (default), RxD (default)
 */
#define TX_LOG_ENABLED
#define RX_LOG_ENABLED

#ifdef TX_LOG_ENABLED
#define TX_LOG(msg, ...)  ESP_LOGI("TX:", msg, ##__VA_ARGS__);
#else
#define TX_LOG(msg, ...)
#endif

#ifdef RX_LOG_ENABLED
#define RX_LOG(msg, ...)  ESP_LOGI("\t\t\t\tRX:", msg, ##__VA_ARGS__);
#else
#define RX_LOG(msg, ...)
#endif


#define EX_UART_NUM UART_NUM_2
#define PATTERN_CHR_NUM    (3)         /*!< Set the number of consecutive and identical characters received by receiver which defines a UART pattern*/

#define BUF_SIZE (1024)
#define RD_BUF_SIZE (BUF_SIZE)
#define PACKET_QUEUE_SIZE 10

static QueueHandle_t uart0_queue;
static QueueHandle_t packet_queue;

typedef enum {
	MS_NONE,
	MS_55,
	MS_F0,
	MS_ADDRESS,
	MS_CMD,
	MS_DLEN,
	MS_DATA,
	MS_CRC_LOW,
	MS_CRC_HIGH
} motorola_state_t;


typedef struct {
	uint32_t received_packets;
	uint32_t sent_packets;
	uint32_t fifo_ovf;
	uint32_t send_errors;
	uint32_t receive_buffer_full;
	uint32_t framing_errors;
	uint32_t rx_packet_ovf;
	uint32_t tx_packet_ovf;
	uint32_t cmd_errors;
	uint32_t dlen_errors;
	uint32_t crc_errors;
} motorola_statistics_t;

#define MAX_CHANNEL (8)
#define CMD_START (44)
#define CMD_STOP (45)

#define MAX(a,b) ((a)<(b)?(b):(a))

typedef struct {
	uint8_t time_sec;
	uint16_t value;
} channel_meas_t;

typedef struct {
	channel_meas_t channels[MAX_CHANNEL];
} device_meas_t;

typedef struct {
	uint8_t channel;
	uint8_t time_sec;
} start_meas_cmd_t;

typedef struct {
	uint8_t channel;
} stop_meas_cmd_t;

typedef union {
	start_meas_cmd_t start_meas;
	stop_meas_cmd_t stop_meas;
} request_cmd_t;

typedef union {
	uint8_t cmd;
	uint8_t status;
} cmd_ack_t;

typedef union {
	request_cmd_t rx_cmd;
	cmd_ack_t ack;
	uint8_t as_byte[MAX(sizeof(cmd_ack_t), sizeof(request_cmd_t))];
} motorola_data_t;

typedef struct {
	uint8_t cmd;
	uint8_t dlen;
	motorola_data_t data;
} motorola_receive_t;

static motorola_state_t state=MS_NONE;
static motorola_statistics_t statistics;
static const char header[]={ 0x55, 1, 0xf0 };
static uint8_t address=42;
static uint8_t dtmp[RD_BUF_SIZE];


static inline BaseType_t send_crc(crc16_t crc) {
	uint8_t c=crc & 0xff;
	TX_LOG("crc low:%x", c);
	if (uart_write_bytes(EX_UART_NUM, &c, 1)!=1)
			return pdFAIL;

	c=(crc>>8) & 0xff;
	TX_LOG("crc high:%x", c);
	return uart_write_bytes(EX_UART_NUM, &c, 1)==1 ? pdPASS:pdFAIL;
}

static inline BaseType_t send_cmd_start(motorola_data_t *data, crc16_t *crc) {
	uint8_t dlen=sizeof(start_meas_cmd_t);
	if (uart_write_bytes(EX_UART_NUM, &dlen, 1)!=1)
		return pdFAIL;

	crc_add_byte(crc, dlen);
	TX_LOG("dlen: %x, crc:%x", dlen, *crc);

	for (int i=0;i<dlen; ++i) {
		uint8_t c=data->as_byte[i];
		if (uart_write_bytes(EX_UART_NUM, &c, 1)!=1)
			return pdFAIL;
		crc_add_byte(crc, c);
		TX_LOG("data[%d]: %x, crc:%x", i, c, *crc);
	}

	return  send_crc(*crc);
}

static inline BaseType_t send_cmd_stop(motorola_data_t *data, crc16_t *crc) {
	uint8_t dlen=sizeof(stop_meas_cmd_t);
	if (uart_write_bytes(EX_UART_NUM, &dlen, 1)!=1)
		return pdFAIL;

	crc_add_byte(crc, dlen);
	TX_LOG("dlen: %x, crc:%x", dlen, *crc);

	for (int i=0;i<dlen; ++i) {
		uint8_t c=data->as_byte[i];
		if (uart_write_bytes(EX_UART_NUM, &c, 1)!=1)
			return pdFAIL;
		crc_add_byte(crc, c);
		TX_LOG("data[%d]: %x, crc:%x", i, c, *crc);

	}
	return  send_crc(*crc);
}

BaseType_t motorola_send(motorola_data_t *data, uint8_t cmd, uint8_t recipient) {
	TX_LOG("==send, cmd:%d, recipient:%d==", cmd, recipient);
	if (!data)
		return pdFAIL;
	if (!(cmd == CMD_START || cmd==CMD_STOP))
		return pdFAIL;

	//send header 0x55, 1, 0xf0
	if (uart_write_bytes(EX_UART_NUM, header, sizeof(header)) != sizeof(header))
		return pdFAIL;

	crc16_t crc;
	crc_begin(&crc);

	// send recipient
	if (uart_write_bytes(EX_UART_NUM, &recipient, 1) != 1)
		return pdFAIL;

	crc_add_byte(&crc, recipient);
	TX_LOG("recipient: %x, crc:%x", recipient, crc);

	//send command
	if (uart_write_bytes(EX_UART_NUM, &cmd, 1) != 1)
		return pdFAIL;

	crc_add_byte(&crc, cmd);
	TX_LOG("cmd: %x, crc:%x", cmd, crc);

	switch (cmd) {
	case CMD_START : return send_cmd_start(data, &crc);

	case CMD_STOP : return send_cmd_stop(data, &crc);

	default:
		ESP_LOGE(TAG, "Unknown command:%d", cmd);
		return pdFAIL;
	}

	return pdFAIL;
}

static uint8_t check_cmd(uint8_t cmd, uint8_t dlen) {
	switch(cmd) {
	case CMD_START : return dlen==sizeof(start_meas_cmd_t);
	case CMD_STOP : return dlen==sizeof(stop_meas_cmd_t);
	default: return 0;
	}
}

static char *get_state_name(motorola_state_t state) {
	switch(state) {
	case MS_NONE: return "none";
	case MS_55: return "55";
	case MS_F0: return "f0";
	case MS_ADDRESS: return "address";
	case MS_CMD: return "cmd";
	case MS_DLEN: return "dlen";
	case MS_DATA: return "data";
	case MS_CRC_LOW: return "crc_low";
	case MS_CRC_HIGH: return "crc_high";
	default: return "UNKNOWN!";
	}
}

static void read_motorola(uint8_t *data, uint8_t dlen) {
	if (!data&&dlen)
		return;
	//RX_LOG("Enter read_motorola, dlen:%d", dlen);

	static motorola_receive_t receive_packet;
	static crc16_t calculated_rec_crc;
	static crc16_t sent_crc;
	static uint8_t cindex;

	for (int i=0;i<dlen;i++) {
		uint8_t c=*(data+i);
		RX_LOG("state:%s, c:%x", get_state_name(state), c);
		switch(state) {
		case MS_NONE:
			if (c==0x55)
				state=MS_55;
			continue;

		case MS_55:
			state=c==1
				? MS_F0
				: c==0x55
				  	  ? MS_55
				  	  : MS_NONE;
			continue;

		case MS_F0:
			state=c==(uint8_t)0xf0 ? MS_ADDRESS : MS_NONE;
			continue;

		case MS_ADDRESS :
			++statistics.received_packets;
			if (c!=address) {
				state=MS_NONE;
				continue;
			}
			crc_begin(&calculated_rec_crc);
			crc_add_byte(&calculated_rec_crc, c);
			RX_LOG("address:%x, crc:%x", c, calculated_rec_crc);
			state = MS_CMD;
			continue;

		case MS_CMD:
			state=MS_DLEN;
			receive_packet.cmd=c;
			crc_add_byte(&calculated_rec_crc, c);
			RX_LOG("command:%x, crc:%x", c, calculated_rec_crc);
			continue;

		case MS_DLEN :
			if (!check_cmd(receive_packet.cmd, c)) {
				state=MS_NONE;
				statistics.cmd_errors++;
				break;
			}
			crc_add_byte(&calculated_rec_crc, c);
			RX_LOG("dlen:%x, crc:%x", c, calculated_rec_crc);
			receive_packet.dlen=c;
			cindex=0;
			state = receive_packet.dlen? MS_DATA : MS_CRC_LOW;
			continue;

		case MS_DATA:
			RX_LOG("dlen:%d, data[%d]:%x, crc:%x", receive_packet.dlen, cindex, c, calculated_rec_crc);
			receive_packet.data.as_byte[cindex++]=c;
			crc_add_byte(&calculated_rec_crc, c);
			if (cindex>=receive_packet.dlen) {
				RX_LOG("switch to crc_low, cindex:%d", cindex);
				state=MS_CRC_LOW;
			}
			continue;

		case MS_CRC_LOW:
			RX_LOG("Crc low:%x", c);
			sent_crc=c;
			state=MS_CRC_HIGH;
			continue;

		case MS_CRC_HIGH:
			RX_LOG("Crc hi c:%x", c);
			sent_crc|=(c<<8);

			if (calculated_rec_crc!=sent_crc){
				ESP_LOGW(TAG, "CRC Error crc:%x", calculated_rec_crc);
				++statistics.crc_errors;
				break;
			}
			if (xQueueSend(packet_queue, &receive_packet, pdMS_TO_TICKS(10)) != pdPASS) {
				statistics.rx_packet_ovf++;
				ESP_LOGE(TAG, "Receive packet queue overflow!");
			}
			RX_LOG("Valid packet incoming");
			state=MS_NONE;
			continue;
		} // switch
		state=MS_NONE;
		statistics.framing_errors++;
	} //for
}


static void uart_event_task(void *pvParameters)
{
    uart_event_t event;
    size_t buffered_size;

    for(;;) {
        //Waiting for UART event.
        if(xQueueReceive(uart0_queue, (void * )&event, pdMS_TO_TICKS(10))) {
            bzero(dtmp, RD_BUF_SIZE);
            switch(event.type) {
                //Event of UART receving data
                /*We'd better handler data event fast, there would be much more data events than
                other types of events. If we take too much time on data event, the queue might
                be full.*/
                case UART_DATA:
                    uart_read_bytes(EX_UART_NUM, dtmp, event.size, portMAX_DELAY);
                    read_motorola(dtmp, event.size);  //!!
                    break;
                //Event of HW FIFO overflow detected
                case UART_FIFO_OVF:
                    ESP_LOGI(TAG, "hw fifo overflow");
                    // If fifo overflow happened, you should consider adding flow control for your application.
                    // The ISR has already reset the rx FIFO,
                    // As an example, we directly flush the rx buffer here in order to read more data.
                    uart_flush_input(EX_UART_NUM);
                    xQueueReset(uart0_queue);
                    break;
                //Event of UART ring buffer full
                case UART_BUFFER_FULL:
                    ESP_LOGI(TAG, "ring buffer full");
                    // If buffer full happened, you should consider encreasing your buffer size
                    // As an example, we directly flush the rx buffer here in order to read more data.
                    uart_flush_input(EX_UART_NUM);
                    xQueueReset(uart0_queue);
                    break;
                //Event of UART RX break detected
                case UART_BREAK:
                    ESP_LOGI(TAG, "uart rx break");
                    break;
                //Event of UART parity check error
                case UART_PARITY_ERR:
                    ESP_LOGI(TAG, "uart parity error");
                    break;
                //Event of UART frame error
                case UART_FRAME_ERR:
                    ESP_LOGI(TAG, "uart frame error");
                    break;
                //UART_PATTERN_DET
                case UART_PATTERN_DET:
                    uart_get_buffered_data_len(EX_UART_NUM, &buffered_size);
                    int pos = uart_pattern_pop_pos(EX_UART_NUM);
                    ESP_LOGI(TAG, "[UART PATTERN DETECTED] pos: %d, buffered size: %d", pos, buffered_size);
                    if (pos == -1) {
                        // There used to be a UART_PATTERN_DET event, but the pattern position queue is full so that it can not
                        // record the position. We should set a larger queue size.
                        // As an example, we directly flush the rx buffer here.
                        uart_flush_input(EX_UART_NUM);
                    } else {
                        uart_read_bytes(EX_UART_NUM, dtmp, pos, 100 / portTICK_PERIOD_MS);
                        uint8_t pat[PATTERN_CHR_NUM + 1];
                        memset(pat, 0, sizeof(pat));
                        uart_read_bytes(EX_UART_NUM, pat, PATTERN_CHR_NUM, 100 / portTICK_PERIOD_MS);
                        ESP_LOGI(TAG, "read data: %s", dtmp);
                        ESP_LOGI(TAG, "read pat : %s", pat);
                    }
                    break;
                //Others
                default:
                    ESP_LOGI(TAG, "uart event type: %d", event.type);
                    break;
            }
        }
    }
    vTaskDelete(NULL);
}

static inline void print_received_data(motorola_receive_t *received) {
	if (!received)
		return;

	ESP_LOGI(TAG,"Incoming received packet, cmd:%d", received->cmd);
	switch(received->cmd) {
	case CMD_START : ESP_LOGI(TAG,"dlen:%d, channel:%d, time:%d", received->dlen,
							 received->data.rx_cmd.start_meas.channel,
							 received->data.rx_cmd.start_meas.time_sec);
	return;

	case CMD_STOP : ESP_LOGI(TAG,"dlen:%d, channel:%d", received->dlen, received->data.rx_cmd.stop_meas.channel);
	return;

	default: ESP_LOGW(TAG, "Unknown command:%d", received->cmd);
	}
}

static void send_test(void *p) {
	vTaskDelay(pdMS_TO_TICKS(100));
	motorola_data_t data;
	motorola_receive_t received;
	for(;;) {
		data.rx_cmd.start_meas.channel=0;
		data.rx_cmd.start_meas.time_sec=42;
		motorola_send(&data, CMD_START, 42);
		vTaskDelay(pdMS_TO_TICKS(500));
		data.rx_cmd.stop_meas.channel=0;
		motorola_send(&data, CMD_STOP, 42);
		vTaskDelay(pdMS_TO_TICKS(500));
		if(xQueueReceive(packet_queue, &received, pdMS_TO_TICKS(10))==pdTRUE) {
			print_received_data(&received);
		}
	}
}

static void rx_test(void *p) {
	motorola_receive_t rx;
	for(;;) {
		if (xQueueReceive(packet_queue,&rx, pdMS_TO_TICKS(10)) == pdTRUE) {
			switch(rx.cmd) {
			case CMD_START: ESP_LOGI(TAG,
									 "START command received, channel:%d, time:%d",
									 rx.data.rx_cmd.start_meas.channel,
									 rx.data.rx_cmd.start_meas.time_sec);
				break;

			case CMD_STOP: ESP_LOGI(TAG,
									"STOP command received, channel:%d",
									rx.data.rx_cmd.stop_meas.channel);
				break;
			default:
				ESP_LOGW(TAG, "Unknown command received:%d", rx.cmd);
			}
		}
	}
}

void app_main(void)
{
    esp_log_level_set(TAG, ESP_LOG_INFO);
    packet_queue=xQueueCreate(PACKET_QUEUE_SIZE, sizeof(motorola_receive_t)); //!!
    configASSERT(packet_queue);  //!!

    /* Configure parameters of an UART driver,
     * communication pins and install the driver */
    uart_config_t uart_config = {
        .baud_rate = 115200,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
        .source_clk = UART_SCLK_APB,
    };
    //Install UART driver, and get the queue.
    uart_driver_install(EX_UART_NUM, BUF_SIZE * 2, BUF_SIZE * 2, 20, &uart0_queue, 0);
    uart_param_config(EX_UART_NUM, &uart_config);


    //Set UART pins (using UART0 default pins ie no changes.)
    uart_set_pin(EX_UART_NUM, GPIO_NUM_17, GPIO_NUM_16, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);

    //Set uart pattern detect function.
    //uart_enable_pattern_det_baud_intr(EX_UART_NUM, '+', PATTERN_CHR_NUM, 9, 0, 0);
    //Reset the pattern queue length to record at most 20 pattern positions.
    //uart_pattern_queue_reset(EX_UART_NUM, 20);

    //Create a task to handler UART event from ISR
    xTaskCreate(uart_event_task, "uart_event_task", 2048, NULL, uxTaskPriorityGet(NULL), NULL);

    xTaskCreate(send_test, "mots", 2048, NULL, uxTaskPriorityGet(NULL), NULL);

    xTaskCreate(rx_test, "motrx", 2048, NULL, uxTaskPriorityGet(NULL), NULL);
}
