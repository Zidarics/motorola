/*
 * crc.h
 *
 *  Created on: Sep 27, 2022
 *      Author: zamek
 */

#ifndef MAIN_CRC_H_
#define MAIN_CRC_H_

#include <stdint.h>
#include <stddef.h>

typedef uint16_t crc16_t;

crc16_t crc_calc_buffer(char *data, size_t size);

void crc_add_byte(crc16_t *crc, char byte);

void crc_modbus_add_byte(crc16_t *crc, char byte);

void crc_begin(crc16_t *crc);

void crc_modbus_begin(crc16_t *crc);

#endif /* MAIN_CRC_H_ */
