/*
 * macros.h
 *
 *  Created on: Sep 27, 2022
 *      Author: zamek
 */

#ifndef MAIN_MACROS_H_
#define MAIN_MACROS_H_

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define MALLOC(size,ptr)    \
	do {					\
		ptr=malloc(size);   \
		assert(ptr);    	\
	} while(0)

#define FREE(ptr)			\
	do { 					\
		free(ptr);			\
		ptr=NULL;			\
	} while(0)

#define CHECK_ERROR_CODE(returned, expected, msg) 	\
	do {											\
		if (returned != expected) {					\
			printf(msg);							\
			printf(", returned:%d, expected:%d\n", (int) returned, (int) expected); \
			abort();								\
		}											\
	} while(0)


#define BOOL_PRINT(cond) cond?"true":"false"

#endif /* MAIN_MACROS_H_ */
